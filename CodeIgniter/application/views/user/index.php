<!-- registro -->



<h1>Registration Form</h1>
<!-- Nav Tabs -->
<ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="nav-item">
        <a class="nav-link" href="#register" aria-controls="register" role="tab" data-toggle="tab">Register</a>
    </li>
    <li role="presentation" class="nav-item">
        <a class="nav-link" href="#list" aria-controls="list" role="tab" data-toggle="tab">List</a>
    </li>
</ul>

<!-- Tab Panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="register">
        <div class="row">
            <div class="col-md-7">
                <form role="form" method="POST" action="user/insert">
                    <div class="form-group">
                        <label for="name"> Name </label>
                        <input type="text" class="form-control small" id="name" name="name">
                    </div>
                    <div class="form-group">
                        <label for="lastname"> Lastname </label>
                        <input type="text" class="form-control" id="lastname" name="lastname">
                    </div>
                    <div class="form-group">
                        <label for="username"> Username </label>
                        <input type="text" class="form-control" id="username" name="username">
                    </div>
                    <div class="form-group">
                        <label for="age"> Age </label>
                        <input type="text" class="form-control" id="age" name="age">
                    </div>
                    <button type="submit" class="btn btn-primary" name="save"> Submit </button>
                </form>
            </div>
            <div class="col-md-5">

            </div>
        </div>

    </div>
    <div role="tabpanel" class="tab-pane" id="list">
        <table class="table table-striped table-dark">
            <thead>
                <th> ID </th>
                <th> Name </th>
                <th> Lastname </th>
                <th> Username </th>
                <th> Age </th>
                <th> Actions </th>
            </thead>
            <tbody>
                <?php
                foreach ($list as $value) { ?>
                    <tr>
                        <td> <?php echo $value->id ?> </td>
                        <td> <?php echo $value->name ?> </td>
                        <td> <?php echo $value->lastname ?> </td>
                        <td> <?php echo $value->username ?> </td>
                        <td> <?php echo $value->age ?> </td>
                        <td>
                            <center>
                                <a href="<?php echo site_url('user/update/').$value->id; ?> "> Edit </a>
                                <a href="<?php echo site_url('user/deleteUser/').$value->id; ?> "> Delete </a>
                            </center>
                        </td>
                    </tr>
                <?php }
                ?>
            </tbody>
        </table>
    </div>
</div>