<form role="form" method="POST" action="user/edit">
    <?php foreach ($datosEdit as $value) { ?>

        <input type="hidden" name="id" id="id" value=" <?php echo $value->id ?> ">

        <div class="form-group">
            <label for="name"> Name </label>
            <input type="text" class="form-control small" id="name" name="name" value=" <?php echo $value->name ?> ">
        </div>
        <div class="form-group">
            <label for="lastname"> Lastname </label>
            <input type="text" class="form-control" id="lastname" name="lastname" value=" <?php echo $value->lastname ?> ">
        </div>
        <div class="form-group">
            <label for="username"> Username </label>
            <input type="text" class="form-control" id="username" name="username" value=" <?php echo $value->username ?> ">
        </div>
        <div class="form-group">
            <label for="age"> Age </label>
            <input type="text" class="form-control" id="age" name="age" value=" <?php echo $value->age ?> ">
        </div>
    <?php
    }
    ?>
    <button type="submit" class="btn btn-primary" name="save"> Update </button>
</form>