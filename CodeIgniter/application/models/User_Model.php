<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function listData()
  {
    $query = $this->db->query("SELECT * FROM users");
    return $query->result();
  }

  public function insert($name, $lastname, $username, $age)
  {
    $arrayDatos = array(
      'name' => $name,
      'lastname' => $lastname,
      'username' => $username,
      'age' => $age
    );
    $this->db->insert('users', $arrayDatos);
  }

  public function list()
  {
    $query = $this->db->query("SELECT * FROM users");
    return $query->result();
  }

  public function deleteUser($id)
  {
    $this->db->where('id', $id);
    $this->db->delete('users');
  }

  public function cargar($id) {
    $query = $this->db->query("SELECT * FROM users WHERE id = $id");
    return $query->result();
  }

  public function update($id, $name, $lastname, $username, $age) {
    $array = array(
      'name' => $name,
      'lastname' => $lastname,
      'username' => $username,
      'age' => $age
    );
    $this->db->where('id', $id);
    $this->db->update("users", $array);
  }
}
