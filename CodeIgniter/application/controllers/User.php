<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model("User_Model");
    $this->load->helper('url');
  }

  public function index()
  {
    $data['contenido'] = "user/index";
    $data['listData'] = $this->User_Model->listData();
    $data['list'] = $this->User_Model->list();
    $this->load->view("plantilla", $data);
  }

  public function insert()
  {
    $datos = $this->input->post();
    if (isset($datos)) {
      $name = $datos['name'];
      $lastname = $datos['lastname'];
      $username = $datos['username'];
      $age = $datos['age'];
      $this->User_Model->insert($name, $lastname, $username, $age);
      redirect('');
    }
  }

  public function deleteUser($id = NULL)
  {
    if ($id != NULL) {
      $this->User_Model->deleteUser($id);
      redirect('');
    }
  }

  public function update($id = NULL)
  {
    if ($id != NULL) {
      $data['contenido'] = "user/update";
      $data['listData'] = $this->User_Model->listData();
      $data['datosEdit'] = $this->User_Model->cargar($id);
      $this->load->view('plantilla', $data);
    } else {
      redirect('');
    }
  }

  public function edit() {
    $datos = $this->input->post();
    if (isset($datos)) {
      $id = $datos['id'];
      $name = $datos['name'];
      $lastname = $datos['lastname'];
      $username = $datos['username'];
      $age = $datos['age'];
      $this->User_Model->update($id, $name, $lastname, $username, $age);
      redirect('');
    }
  }
}
